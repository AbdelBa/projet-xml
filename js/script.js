/*********************************************/
/************ Fonctions AJAX *****************/
/*********************************************/
function onChangeForm(form)
{
    var querystring = getQueryString(form);

    // Mise à jour des cartes SVG et Html5.
    var svgUrl = form.action + "get-data-svg.php" + querystring;
    var jsonUrl = form.action + "get-data-json.php" + querystring;

    makeRequest(svgUrl, updateSvgContent);
    makeRequest(jsonUrl, updateHtml5Content);

    // Mise à jour du formulaire.
    var lastSpecifiedElement = getLastSpecifiedElement(form);
    var formUrl = form.action + "get-data-form.php" + lastSpecifiedElement;

    makeRequest(formUrl, updateForm);
}

function makeRequest(url, callback) 
{
    var httpRequest = new XMLHttpRequest();
    if (httpRequest.overrideMimeType) 
    {
        httpRequest.overrideMimeType('text/xml');
    }

    if (!httpRequest) 
    {
        alert('Abandon :( Impossible de créer une instance XMLHTTP');
        return false;
    }

    httpRequest.onreadystatechange = function() 
    {
        if (httpRequest.readyState == 4) 
        {
            if (httpRequest.status == 200) 
            {
                callback(httpRequest.responseText);
            }
            else 
            {
                alert('Un problème est survenu avec la requête.');
            }
        }
    };
    httpRequest.open('GET', url, true);
    httpRequest.send();
}

/*********************************************/
/************ Fonctions html5 canvas 2d ******/
/*********************************************/

// Appelée lorsque la page est chargée, s'occupe du canvas Html5.
function initHtml5Canvas()
{
    var canvas = document.getElementById('html5canvas');
    if (canvas.getContext) 
    {
        ctx = canvas.getContext("2d");
        ctx.pins = [];
        ctx.texts = [];
        ctx.imgs = [];
        ctx.titles = [];
        pinImg = new Image();
        pinImg.src = 'img/pin-small.png';

        img = new Image();
        img.onload = function()
        {
            ctx.drawImage(img, 0, 0);
        };
        img.src = 'img/map.png'; 
    }
}

function mouseMoveCanvas(event) 
{
    var canvas = document.getElementById('html5canvas');

    x = event.offsetX ? (event.offsetX): event.pageX - canvas.offsetLeft; 
    y = event.offsetY ? (event.offsetY): event.pageY - canvas.offsetTop;

    for (var i = 0; i < ctx.pins.length; i++) 
    {
        if (ctx.pins[i][0] > x-3 && ctx.pins[i][0] < x+3 && ctx.pins[i][1] > y+10 && ctx.pins[i][1] < y+16)
        {
            updateInfo(ctx.titles[i], ctx.texts[i], ctx.imgs[i]);
        }
    }
}

function resetCanvas()
{
    ctx.clearRect (0 , 0, 850, 540);
    ctx.drawImage(img, 0, 0);
    ctx.pins = [];
    ctx.texts = [];
    ctx.imgs = [];
    ctx.titles = [];
}

function updateHtml5Content(json)
{
    var objects = JSON.parse(String(json));
    resetCanvas();
    objects.forEach(drawPin);
}

function drawPin(jsonElement)
{
    ctx.drawImage(pinImg, jsonElement.x-10, jsonElement.y-20, 13, 20);
    ctx.pins.push([parseInt(jsonElement.x), parseInt(jsonElement.y)]);
    ctx.imgs.push(jsonElement.image);
    ctx.texts.push(jsonElement.text);
    ctx.titles.push(jsonElement.nom);
}

/*********************************************/
/************ Fonctions SVG ******************/
/*********************************************/
function updateSvgContent(content)
{
    document.getElementById("svg").innerHTML = content;
}

/*********************************************/
/************ Fonctions pour le formulaire ***/
/*********************************************/
function getQueryString(form)
{
    var querystring = '?';

    for (var i=0;i<form.elements.length;i++)
    {
        querystring += form.elements.item(i).name + '=' +
                       form.elements.item(i).value + '&';
    }
    
    return querystring;
}

function getLastSpecifiedElement(form)
{
    var lastSpecifiedElement = '';

    for (var i=0;i<form.elements.length;i++)
    {
        if (form.elements.item(i).value != 'indifferent')
        {
            lastSpecifiedElement = '?' + form.elements.item(i).name + '=' +
                                         form.elements.item(i).value + '&';
        }
    }

    return lastSpecifiedElement;
}


function updateForm(json)
{
    var objects = JSON.parse(String(json));
    var form = document.getElementById('main-form');

    for (var i=1;i<form.elements.length;i++)
    {
        var val = form.elements.item(i).value
        form.elements.item(i).innerHTML = '<option id="indifferent" value="indifferent">Indifférent</option>';

        if (objects[i])
        {
            for (var j=0;j<objects[i].length;j++)
            {
                form.elements.item(i).innerHTML+= '<option id='+ objects[i][j] + ' value="' + objects[i][j] + '">' + objects[i][j] + '</option>';
            }
        }

        // Pour remettre l'ancienne valeur selectionnée.
        var index = form.elements.item(i).options.namedItem(val.split(' ')[0]).index;
        if (index)
        {
            form.elements.item(i).selectedIndex = index;
        }
    }

    // Suppression des éventuels doublons.
    $("select>option").each( function(){
     var $option = $(this);  

     $option.siblings()
           .filter( function(){ return $(this).val() == $option.val() } )
           .remove()})
}
/*********************************************/
/************ Fonctions graphiques ***********/
/*********************************************/

//Appelée lorsque l'on passe la souris sur un repère sur la carte.
function updateInfo(title, text, img)
{
    $('#info-title').hide().html(title).slideDown('slow');
    $('#info-img').hide().html('<img class="thumb" src=img/' + img + '>').fadeIn('slow');
    $('#info-text').hide().html(text).slideDown('slow');
}

$(document).ready(function()
{
    $('a[href^="#"]').click(function(){  
        var the_id = $(this).attr("href");  
        $('html, body').animate({  
            scrollTop:$(the_id).offset().top - 130
            }, 'slow');  
            return false;  
        })
});
