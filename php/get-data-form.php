<?php
header('Content-type: application/json');

$doc_xml = new DOMDocument();
$doc_xml->load('../data.xml');

$doc_xsl = new DOMDocument();
$doc_xsl->load('../xsl/update-form.xsl');

$proc = new XsltProcessor();
$proc->importStylesheet($doc_xsl);

$proc->setParameter(null, 'filter', urldecode($_SERVER['QUERY_STRING']));

echo $proc->transformToXml($doc_xml);
?>
