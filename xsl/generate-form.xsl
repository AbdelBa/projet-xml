<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" encoding="UTF-8" />
<xsl:strip-space elements="*" />


<xsl:template match="/">
    <html lang="fr">
        <head>
            <meta charset="utf-8" />
            <title>Les grands hommes de l'Histoire</title>
            <link href="css/style.css" rel="stylesheet" type="text/css"/>
            <script type="text/javascript" src="js/jquery.js"></script>
            <script type="text/javascript" src="js/script.js"></script>
        </head>
        <body onload="initHtml5Canvas();">
            <div id="nav-bar">
                <div id="inner-nav-bar">
                    <h1 id="heading"><a href="data.xml">Les grands hommes de l'Histoire</a></h1>
                    <ul id="menu">
                        <li><a href="#svg">SVG</a></li>
                        <li><a href="#html5">Html5</a></li>
                    </ul>
                </div>
            </div>
            <div class="form">
                <form id="main-form" method="GET" action="php/">
                    <xsl:apply-templates select="//*" />
                </form>
            </div>

            <div id="info">
                <h2 id="info-title"></h2>
                <p id="info-text"><span class="placeholder">Info : passez la souris sur un élément de la carte pour obtenir des informations.</span></p>
                <p id="info-img"></p>
            </div>

            <div id="svg" class="map">
                <img src="img/map.png"/>
            </div>

            <div id="html5" class="map last-element">
               <canvas id="html5canvas" width="850px" height="540px" onmousemove="mouseMoveCanvas(event)">
                    <img src="img/map.png"/>
               </canvas>
            </div>
        </body>
    </html>
</xsl:template>

<!-- La première balise est traitée différement des autres car elle ne possède qu'un attribut, elle est invisible. On est obligé de la traiter pour qu'elle apparaisse dans la query-string -->
<xsl:template match="/*">
    <xsl:variable name="tagName" select="name()" />
    <xsl:for-each select="@*[not(name(.)='x' or name(.)='y')]">
        <xsl:variable name="attName" select="name()" />
        <xsl:variable name="lstName" select="concat($tagName, '-', $attName)" />
            <select name="{$lstName}" style="display:none">
                <xsl:apply-templates select="//*[name()=$tagName]/@*[name()=$attName]" />
            </select>
    </xsl:for-each>
</xsl:template>

<xsl:template match="*[not(preceding::*[name()=name(current())]) and not(name()=name(/*))]">
    <xsl:variable name="tagName" select="name()" />

    <xsl:for-each select="@*[not(name(.)='x' or name(.)='y' or name(.)='image')]">
        <xsl:variable name="attName" select="name()" />
        <xsl:variable name="lstName" select="concat($tagName, '-', $attName)" />
        <div class="select-container">
            <label for="{$lstName}"><xsl:value-of select="$lstName" /></label><br/>
            <select name="{$lstName}" onchange="onChangeForm(this.form)">
                <option id="indifferent" value="indifferent">
                    Indifférent
                </option>
                <xsl:apply-templates select="//*[name()=$tagName]/@*[name()=$attName]" />
            </select>
        </div>
    </xsl:for-each>
</xsl:template>

<xsl:template match="@*[not(preceding::*/@*[. = current()])]">
    <option id="{.}" value="{.}">
        <xsl:value-of select="."/>
    </option>
</xsl:template>

<xsl:template match="@*"></xsl:template>
<xsl:template match="*"></xsl:template>

</xsl:stylesheet>
