<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:dyn="http://exslt.org/dynamic">
<xsl:output method="text" encoding="UTF-8" />
<xsl:strip-space elements="*" />

<xsl:param name="filter" />

<xsl:template match="/">
    <xsl:variable name="xpath">
        <xsl:call-template name="toXpath">
            <xsl:with-param name="queryString" select="$filter" />
        </xsl:call-template>
    </xsl:variable>
    [
        <xsl:for-each select="dyn:evaluate($xpath)">
            {
                <xsl:for-each select="@*">
                    "<xsl:value-of select="name()"/>" : "<xsl:value-of select="."/>",
                </xsl:for-each>
                "text" : "<xsl:value-of select="."/>"
            }
            <xsl:if test="not(position() = last())">,</xsl:if>
        </xsl:for-each>
    ]
</xsl:template>

<xsl:template match="*">
</xsl:template>

<xsl:template name="toXpath">
    <xsl:param name="queryString" />
    <xsl:param name="previousTag" select="''" />

    <xsl:if test="$queryString">
        <xsl:variable name="car" select="substring-before($queryString, '&amp;')" />
        <xsl:variable name="cdr" select="substring-after($queryString, '&amp;')" />
        
        <xsl:variable name="tag" select="substring-before($car, '-')" />
        <xsl:variable name="att" select="substring-before(substring-after($car, '-'), '=')" />
        <xsl:variable name="val" select="translate(substring-after($car, '='), '+', ' ')" />

        <xsl:choose>
            <xsl:when test="$tag = $previousTag and $val != 'indifferent'">
                <xsl:value-of select="concat('[@', $att, '=', '&quot;', $val, '&quot;', ']')"/> 
            </xsl:when>

            <xsl:when test="$tag != $previousTag and $val != 'indifferent'">
                <xsl:value-of select="concat('/', $tag, '[@', $att, '=', '&quot;' , $val, '&quot;', ']')" /> 
            </xsl:when>

            <xsl:when test="$tag != $previousTag and $val = 'indifferent'">
                <xsl:value-of select="concat('/', $tag)" />
            </xsl:when>

            <xsl:otherwise>
            </xsl:otherwise> 
        </xsl:choose>
        
        <xsl:call-template name="toXpath">
            <xsl:with-param name="queryString" select="$cdr" />
            <xsl:with-param name="previousTag" select="$tag" />
        </xsl:call-template>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>
