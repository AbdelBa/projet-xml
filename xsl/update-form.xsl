<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:dyn="http://exslt.org/dynamic">
<xsl:output method="text" encoding="UTF-8" />
<xsl:strip-space elements="*" />

<xsl:param name="filter" />

<xsl:template match="/">
    <xsl:variable name="xpath">
        <xsl:call-template name="toXpath">
            <xsl:with-param name="queryString" select="$filter" />
        </xsl:call-template>
    </xsl:variable>

    <xsl:for-each select="dyn:evaluate($xpath)">
        [
            <!-- D'abord les ancêtres -->
            <xsl:for-each select="ancestor::*">
            [
                <xsl:for-each select="./preceding-sibling::* | . | ./following-sibling::*">
                    "<xsl:value-of select="@nom"/>"
                    <xsl:if test="not(position() = last())">,</xsl:if>
                </xsl:for-each>
            ],
            </xsl:for-each>

            <!-- Ensuite soi-même -->
            [
                <xsl:for-each select="./preceding-sibling::* | . | ./following-sibling::*">
                    "<xsl:value-of select="@nom"/>"
                    <xsl:if test="not(position() = last())">,</xsl:if>
                </xsl:for-each>
            ]

            <!-- Et après les descendants, on doit effectuer un parcours en largeur -->
                <xsl:call-template name="breathFirst">
                    <xsl:with-param name="startNode" select="." />
                </xsl:call-template>
        ]


    </xsl:for-each>

</xsl:template>

<xsl:template name="breathFirst">
    <xsl:param name="startNode"/>

    <!-- On affiche d'abord le nom, qui est obligatoire -->
    <xsl:if test="$startNode/*">
    ,
    [
        <xsl:for-each select="$startNode/*">
            "<xsl:value-of select="@nom"/>"
            <xsl:if test="not(position() = last())">,</xsl:if>
        </xsl:for-each>
    ]
    </xsl:if>

    <!-- Éventuellement on aura d'autres attributs que le nom -->
    <xsl:if test="$startNode[@*[not(name(.)='x' or name(.)='y' or name(.)='image' or name(.)='nom')]]">
        ,
        [
        <xsl:for-each select="$startNode/@*[not(name(.)='x' or name(.)='y' or name(.)='image' or name(.)='nom')]">
                "<xsl:value-of select="."/>"
            <xsl:if test="not(position() = last())">,</xsl:if>
        </xsl:for-each>
        ]
    </xsl:if>

    <!-- Appel récursif sur les descendants -->
    <xsl:if test="$startNode/*">
        <xsl:call-template name="breathFirst">
            <xsl:with-param name="startNode" select="$startNode/*" />
        </xsl:call-template>
    </xsl:if>

</xsl:template>


<xsl:template match="*">
</xsl:template>

<xsl:template name="toXpath">
    <xsl:param name="queryString" />
    <xsl:param name="previousTag" select="''" />

    <xsl:if test="$queryString">
        <xsl:variable name="car" select="substring-before($queryString, '&amp;')" />
        <xsl:variable name="cdr" select="substring-after($queryString, '&amp;')" />
        
        <xsl:variable name="tag" select="substring-before($car, '-')" />
        <xsl:variable name="att" select="substring-before(substring-after($car, '-'), '=')" />
        <xsl:variable name="val" select="translate(substring-after($car, '='), '+', ' ')" />

        <xsl:value-of select="concat('//', $tag, '[@', $att, '=', '&quot;', $val, '&quot;', ']')" />
    </xsl:if>
</xsl:template>

</xsl:stylesheet>
